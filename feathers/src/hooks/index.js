'use strict';

// Add any common hooks you want to share across services in here.
//
// Below is an example of how a hook is written and exported. Please
// see http://docs.feathersjs.com/hooks/readme.html for more details
// on hooks.

exports.myHook = function(options) {
  return function(hook) {
    console.log('My custom global hook ran. Feathers is awesome!');
  };
};

exports.regex = function (options) {
  return function (hook) {
    const query = hook.params.query;

    Object.keys(query).forEach(key => {
      const value = query[key];
    if (value && value.$regex) {
      value.$regex = new RegExp(value.$regex);
    } else if (key === '$or' && Array.isArray(value)) {
      for (let i = 0; i < value.length; i++) {
        let orItem = value[i];

        for (let orItemKey in orItem) {
          if (value[i][orItemKey] && value[i][orItemKey].$regex) {
            value[i][orItemKey].$regex = new RegExp(value[i][orItemKey].$regex);
          }
        }
      }
    }
  });
  };
};
