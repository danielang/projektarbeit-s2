'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('address service', function() {

  it('registered the addresses service', () => {
    assert.ok(app.service('addresses'));
  });

  it('add a new address', () => {
    assert.ok(app.service('addresses').create({
      lat: 123,
      long: 456,
      name: 'Testadresse',
      street: 'Teststreet',
      housenumber: '1a',
      city: 'Testinghausen',
      zipcode: '12345',
      country: 'Testland'
    }));
  });


});


