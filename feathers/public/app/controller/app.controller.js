sap.ui.define([
  'jquery.sap.global',
  'sap/ui/core/Component',
  'sap/ui/core/mvc/Controller',
  'sap/ui/model/json/JSONModel',
  'sap/m/MessageToast',
  'sap/ui/model/Filter',
  'sap/ui/model/FilterOperator'
], function (jQuery, Component, Controller, JSONModel, MessageToast, Filter, FilterOperator) {
  'use strict';

  var AppController = Controller.extend('app.controller.app', {

    _oNewAddressModel: undefined,

    // ====== init ====================================================================

    onInit: function () {
      // get Component
      this._component = Component.getOwnerComponentFor(this.getView());

      this._oNewAddressModel = new JSONModel({
        lat: '',
        long: '',
        name: '',
        housenumber: '',
        street: '',
        city: '',
        zipcode: '',
        country: ''
      });
      this.oView.setModel(this._oNewAddressModel, 'newaddress');
    },

    // ====== event handler ===========================================================

    onAddressSuggest: function (oEvent) {
      var sValue = oEvent.getParameter("suggestValue");
      var filters = [];
      if (sValue) {
        filters = [new sap.ui.model.Filter([
          new sap.ui.model.Filter('city', sap.ui.model.FilterOperator.Contains, sValue),
          new sap.ui.model.Filter('street', sap.ui.model.FilterOperator.Contains, sValue),
          new sap.ui.model.Filter('zipcode', sap.ui.model.FilterOperator.Contains, sValue),
          new sap.ui.model.Filter('name', sap.ui.model.FilterOperator.Contains, sValue)
        ], false)]; // false = or
      }

      var oSource = oEvent.oSource;
      oSource.getBinding('suggestionItems').filter(filters).then(function (resolved) {
        oSource.suggest();
      });
    },

    onFilterAddresses: function (oEvent) {
      // build filter array
      var aFilter = [];
      var sQuery = oEvent.getParameter("query");
      if (sQuery) {
        aFilter.push(new Filter("city", FilterOperator.Contains, sQuery));
      }

      // filter binding
      var oList = this.getView().byId("addressTable");
      var oBinding = oList.getBinding("items");
      oBinding.filter(aFilter);
    },


    onAddressAdd: function (oEvent) {
      var oAddress = this._oNewAddressModel.getData();

      this._component.models.address.create(oAddress).then(
        function (oFullfilled) {
          MessageToast.show('Success');
        },
        function (oRejected) {
          MessageToast.show('Error');
        }
      );
    },

    onAddressDelete: function (oEvent) {
      var sId = oEvent.mParameters.listItem.getBindingContext('address').getObject()._id;

      this._component.models.address.remove(sId).then(
        function (oFullfilled) {
          MessageToast.show('Address deleted');
        }, function (oRejected) {
          MessageToast.show('Address could not be deleted. Pls. look in the console');
          console.log(oRejected);
        }
      );
    }

  });

  return AppController;

});
