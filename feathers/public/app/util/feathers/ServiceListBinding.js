/*!
 * UI development toolkit for HTML5 (OpenUI5)
 * (c) Copyright 2009-2016 SAP SE or an SAP affiliate company.
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */

// Provides the JSON model implementation of a list binding
sap.ui.define([
  'jquery.sap.global',
  'sap/ui/model/ChangeReason',
  'sap/ui/model/ClientListBinding',
  'sap/ui/model/FilterOperator',
  'sap/ui/model/FilterType',
  'sap/ui/model/Sorter'
], function (jQuery, ChangeReason, ClientListBinding, FilterOperator, FilterType, Sorter) {
  "use strict";

  var FeathersFilterOperator = {
    internalSupport: [FilterOperator.NE, FilterOperator.LT, FilterOperator.LE, FilterOperator.GT, FilterOperator.GE],

    /**
     * FilterOperator not equals
     * @public
     */
    NE: "$ne",

    /**
     * FilterOperator less than
     * @public
     */
    LT: "$lt",

    /**
     * FilterOperator less or equals
     * @public
     */
    LE: "$lte",

    /**
     * FilterOperator greater than
     * @public
     */
    GT: "$gt",

    /**
     * FilterOperator greater or equals
     * @public
     */
    GE: "$gte",

    /**
     * FilterOperator between.
     * When used on strings, the BT operator might not behave intuitively. For example,
     * when filtering a list of Names with BT "A", "B", all Names starting with "A" will be
     * included as well as the name "B" itself, but no other name starting with "B".
     * @public
     */
    BT: function (oValue1, oValue2) {
      throw Error('Between-Filter not implemented in this Model');
    },

    EQ: function (oValue) {
      return oValue
    },

    /**
     * FilterOperator contains
     * @public
     */
    Contains: function (oValue) {
      if (Array.isArray(oValue)) {
        return {
          $in: oValue
        };
      } else {
        return {
          $regex: oValue
        };
      }
    },

    /**
     * FilterOperator starts with
     * @public
     */
    StartsWith: function (oValue) {
      return {
        $regex: '^' + oValue
      };
    },

    /**
     * FilterOperator ends with
     * @public
     */
    EndsWith: function (oValue) {
      return {
        $regex: '.*' + oValue + '$'
      };
    }
  };

  var ServiceListBinding = ClientListBinding.extend('app.util.feathers.ServiceListBinding', {
    constructor: function (oModel, sPath, oContext, aSorters, aFilters, mParameters) {
      ClientListBinding.apply(this, arguments);

      this.bFirstRequest = true;
    }
  });

  ServiceListBinding.prototype.loadData = function (iStartIndex, iLength, bPretend) {
    if (!iStartIndex) {
      iStartIndex = 0;
    }
    if (!iLength) {
      iLength = Math.min(this.getLength(), this.oModel.iSizeLimit);
    }

    // create query parameters
    var oParams = {
      $skip: iStartIndex,
      $limit: iLength
    };

    // add sort params if exist for this binding
    if (this.aSorters) {
      var oSortParams = {};

      for (var i = 0; i < this.aSorters.length; i++) {
        var oSorter = this.aSorters[i];

        oSortParams[oSorter.sPath] = (oSorter.bDescending == true ? -1 : 1);
      }

      oParams['$sort'] = oSortParams;
    }

    // add filters to params
    if (this.aFilters) {
      var createFilter = function (aFilters, oParams) {
        for (var i = 0; i < aFilters.length; i++) {
          var oFilter = aFilters[i];

          if (oFilter._bMultiFilter) {
            if (oFilter.bAnd) {
              jQuery.extend(oParams, createFilter(oFilter.aFilters, oParams));
            } else {
              oParams['$or'] = createFilter(oFilter.aFilters, []);
            }
          } else {
            if (typeof oFilter.fnTest === 'function' && typeof oFilter.oValue1 === 'undefined') {
              throw Error('This Binding doesn\'t support filter functions');
            }

            if (Array.isArray(oParams)) {
              var oFilterProperty = {};

              if (FeathersFilterOperator.internalSupport.indexOf(oFilter.sOperator) >= 0) {
                oFilterProperty[oFilter.sPath][FeathersFilterOperator[oFilter.sOperator]] = oFilter.oValue1;
              } else {
                oFilterProperty[oFilter.sPath] = FeathersFilterOperator[oFilter.sOperator](oFilter.oValue1);
              }

              oParams.push(oFilterProperty);
            } else {
              if (FeathersFilterOperator.internalSupport.indexOf(oFilter.sOperator) >= 0) {
                oParams[oFilter.sPath][FeathersFilterOperator[oFilter.sOperator]] = oFilter.oValue1;
              } else {
                oParams[oFilter.sPath] = FeathersFilterOperator[oFilter.sOperator](oFilter.oValue1);
              }
            }
          }
        }

        return oParams;
      };

      jQuery.extend(oParams, createFilter(this.aFilters, oParams));
      jQuery.extend(oParams, createFilter(this.aApplicationFilters, oParams));

    }

    /**
     * Callback - Handlers
     */
    var fnSuccess = function (oResult) {
      this.iLength = this.iTotalLength = oResult.total;
    }.bind(this);


    // run service query
    return this.oModel.loadData(oParams, fnSuccess);
  };

  ServiceListBinding.prototype.sort = function (aSorters) {
    if (!aSorters) {
      this.aSorters = null;
    } else {
      if (aSorters instanceof Sorter) {
        aSorters = [aSorters];
      }
      this.aSorters = aSorters;
    }

    return ServiceListBinding.prototype.loadData.apply(this).then(
      function () {
        ClientListBinding.prototype.sort.call(this, aSorters);
      }.bind(this)
    );
  };

  ServiceListBinding.prototype.filter = function (aFilters, sFilterType) {
    if (sFilterType == FilterType.Application) {
      this.aApplicationFilters = aFilters || [];
    } else if (sFilterType == FilterType.Control) {
      this.aFilters = aFilters || [];
    } else {
      //Previous behaviour
      this.aFilters = aFilters || [];
      this.aApplicationFilters = [];
    }

    return ServiceListBinding.prototype.loadData.apply(this).then(
      function () {
        ClientListBinding.prototype.filter.call(this, aFilters, sFilterType);
      }.bind(this)
    );
  };

  /**
   * Return contexts for the list or a specified subset of contexts
   * @param {int} [iStartIndex=0] the startIndex where to start the retrieval of contexts
   * @param {int} [iLength=length of the list] determines how many contexts to retrieve beginning from the start index.
   * Default is the whole list length.
   *
   * @return {Array} the contexts array
   * @protected
   */
  ServiceListBinding.prototype.getContexts = function (iStartIndex, iLength) {
    //	Set default values if startindex, threshold or length are not defined
    if (!iStartIndex) {
      iStartIndex = 0;
    }
    if (!iLength) {
      iLength = this.oModel.iSizeLimit;
      if (this.iLength < iLength) {
        iLength = this.iLength;
      }
    }

    var aContexts = this._getContexts(iStartIndex, iLength),
      aContextData = [],
      bLoadContexts = (aContexts.length != iLength && !(aContexts.length >= this.getLength() - iStartIndex)) || this.bFirstRequest;

    if (bLoadContexts) {
      this.loadData(iStartIndex, iLength);
      this.bFirstRequest = false;
      aContexts.dataRequested = true;
    }

    this.iLastLength = iLength;
    this.iLastStartIndex = iStartIndex;


    if (this.bUseExtendedChangeDetection) {
      // Use try/catch to detect issues with cyclic references in JS objects,
      // in this case diff will be disabled.
      try {
        for (var i = 0; i < aContexts.length; i++) {
          aContextData.push(this.getContextData(aContexts[i]));
        }

        //Check diff
        if (this.aLastContextData && iStartIndex < this.iLastEndIndex) {
          aContexts.diff = jQuery.sap.arraySymbolDiff(this.aLastContextData, aContextData);
        }

        this.iLastEndIndex = iStartIndex + iLength;
        this.aLastContexts = aContexts.slice(0);
        this.aLastContextData = aContextData.slice(0);
      } catch (oError) {
        this.bUseExtendedChangeDetection = false;
        jQuery.sap.log.warning("JSONListBinding: Extended change detection has been disabled as JSON data could not be serialized.");
      }
    }

    return aContexts;
  };

  ServiceListBinding.prototype.getLength = function () {
    if (this.iTotalLength >= this.iLength) {
      return this.iTotalLength;
    } else {
      this.iTotalLength = this.iLength;

      return this.iLength;
    }
  };

  ServiceListBinding.prototype._getLength = function () {
    return this.iLength || 0;
  };

  ServiceListBinding.prototype.isLengthFinal = function () {
    return true;
  };

  ServiceListBinding.prototype.getCurrentContexts = function () {
    if (this.bUseExtendedChangeDetection) {
      return this.aLastContexts || [];
    } else {
      return this.getContexts(this.iLastStartIndex, this.iLastLength);
    }
  };

  /**
   * Returns the context data as required for change detection/diff. This may not contain
   * all of the data, but just the key property
   *
   * @private
   */
  ServiceListBinding.prototype.getContextData = function (oContext) {
    if (this.fnGetEntryKey && !this.bDetectUpdates) {
      return this.fnGetEntryKey(oContext);
    } else {
      return JSON.stringify(oContext.getObject());
    }
  };

  /**
   * Get indices of the list
   */
  ServiceListBinding.prototype.updateIndices = function () {
    var i;

    this.aIndices = [];

    for (i in this.oList) {
      this.aIndices.push(i);
    }
  };

  /**
   * Update the list, indices array and apply sorting and filtering
   * @private
   */
  ServiceListBinding.prototype.update = function () {
    var oList = this.oModel._getObject(this.sPath, this.oContext);
    if (oList) {
      this.oList = jQuery.extend(this.bUseExtendedChangeDetection, {}, oList);

      this.updateIndices();
      this.applyFilter();
      this.applySort();
      this.iLength = this._getLength();
    } else {
      this.oList = [];
      this.aIndices = [];
      this.iLength = 0;
    }
  };

  /**
   * Check whether this Binding would provide new values and in case it changed,
   * inform interested parties about this.
   *
   * @param {boolean} bForceupdate
   *
   */
  ServiceListBinding.prototype.checkUpdate = function (bForceupdate) {

    if (this.bSuspended && !this.bIgnoreSuspend && !bForceupdate) {
      return;
    }

    if (!this.bUseExtendedChangeDetection) {
      var oList = this.oModel._getObject(this.sPath, this.oContext);
      if (!jQuery.sap.equal(this.oList, oList) || bForceupdate) {
        this.update();
        this._fireChange({reason: ChangeReason.Change});
      }
    } else {
      var bChangeDetected = false;
      var that = this;

      //If the list has changed we need to update the indices first
      var oList = this.oModel._getObject(this.sPath, this.oContext);
      if (oList && this.oList.length != oList.length) {
        bChangeDetected = true;
      }
      if (!jQuery.sap.equal(this.oList, oList)) {
        this.update();
      }

      //Get contexts for visible area and compare with stored contexts
      var aContexts = this._getContexts(this.iLastStartIndex, this.iLastLength);
      if (this.aLastContexts) {
        if (this.aLastContexts.length != aContexts.length) {
          bChangeDetected = true;
        } else {
          jQuery.each(this.aLastContextData, function (iIndex, oLastData) {
            var oCurrentData = that.getContextData(aContexts[iIndex]);
            if (oCurrentData !== oLastData) {
              bChangeDetected = true;
              return false;
            }
          });
        }
      } else {
        bChangeDetected = true;
      }
      if (bChangeDetected || bForceupdate) {
        this._fireChange({reason: ChangeReason.Change});
      }
    }
  };


  return ServiceListBinding;

});
