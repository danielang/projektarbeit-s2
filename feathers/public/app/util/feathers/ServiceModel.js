sap.ui.define([
  'jquery.sap.global',
  'sap/ui/model/ClientModel',
  'sap/ui/model/Context',
  './ServiceListBinding',
  'sap/ui/model/json/JSONPropertyBinding',
  'sap/ui/model/json/JSONTreeBinding'
], function (jQuery, ClientModel, Context, ServiceListBinding, JSONPropertyBinding, JSONTreeBinding) {
  "use strict";

  var oDefaults = {
    app: undefined,
    servicename: undefined,
    idField: '_id'
  };

  var ServiceModel = ClientModel.extend('app.util.feathers.ServiceModel', {

    constructor: function (mParameters) {
      ClientModel.apply(this, arguments);

      this.oSettings = jQuery.extend(true, {}, oDefaults, mParameters);

      if (!this.oSettings.app) {
        throw Error('Please provide a feathers app.');
      }

      if (!this.oSettings.servicename) {
        throw Error('Please provide a servicename.');
      }

      this.oData = {};

      this.oService = this.oSettings.app.service(this.oSettings.servicename);

      this.oService.on('created', this._onCreated.bind(this));
      this.oService.on('updated', this._onUpdated.bind(this));
      this.oService.on('patched', this._onPatched.bind(this));
      this.oService.on('removed', this._onRemoved.bind(this));
    },

    metadata: {
      publicMethods: ['find', 'get', 'create', 'update', 'patch', 'remove']
    },

    /**
     * Service event handlers
     */
    _onCreated: function (oCreatedObject) {
      debugger;

      var sIdField = this.oSettings.idField;

      this.oData[oCreatedObject[sIdField]] = oCreatedObject;

      this.checkUpdate();
    },

    _onUpdated: function (oUpdatedObject) {
      debugger;

      var sIdField = this.oSettings.idField;

      this.oData[oUpdatedObject[sIdField]] = oUpdatedObject;

      this.checkUpdate();
    },

    _onPatched: function (oPatchedObject) {
      debugger;

      var sIdField = this.oSettings.idField;

      this.oData[oPatchedObject[sIdField]] = oPatchedObject;

      this.checkUpdate();
    },

    _onRemoved: function (oRemovedObject) {
      debugger;

      var sIdField = this.oSettings.idField;

      if (this.oData[oRemovedObject[sIdField]]) {
        delete this.oData[oRemovedObject[sIdField]];

        this.checkUpdate();
      }
    },

    /**
     * Public all service methods
     */
    find: function () {
      return this.oService.find.apply(this.oService, arguments);
    },

    get: function () {
      return this.oService.get.apply(this.oService, arguments);
    },

    create: function () {
      return this.oService.create.apply(this.oService, arguments);
    },

    update: function () {
      return this.oService.update.apply(this.oService, arguments);
    },

    patch: function () {
      return this.oService.patch.apply(this.oService, arguments);
    },

    remove: function () {
      return this.oService.remove.apply(this.oService, arguments);
    },

    /**
     * Internal functions
     */
    loadData: function (oParams, fnSuccess, fnError, fnCompleted) {
      if (!oParams) {
        oParams = {};
      }

      this.fireRequestSent();

      return this.oService.find({
        query: oParams
      }).then(
        function (oResult) {
          var oData = oResult.data;
          var sIdField = this.oSettings.idField;

          if (jQuery.isArray(oData)) {
            for (var i = 0; i < oData.length; i++) {
              var oDataItem = oData[i];

              if (!this.oData.hasOwnProperty(oDataItem[sIdField])) {
                this.oData[oDataItem[sIdField]] = oDataItem;
              }
            }
          } else {
            throw Error('the requested data was not an array');
          }

          // fire loading succeded
          if (fnSuccess && typeof fnSuccess === 'function') {
            fnSuccess(oResult);
          }

          // update model & bindings
          this.checkUpdate();

          // fire loading completed
          if (fnCompleted && typeof fnCompleted === 'function') {
            fnCompleted(oResult);
          }
          this.fireRequestCompleted();

          return oData;
        }.bind(this),
        function (oError) {
          if (fnError && typeof fnError === 'function') {
            fnError(oError);
          }

          this.fireRequestFailed(oError);

          return oError;
        }.bind(this)
      );
    }

  });


  ServiceModel.prototype.getData = function () {
    return this.oData;
  };


  ServiceModel.prototype.bindProperty = function (sPath, oContext, mParameters) {
    var oBinding = new JSONPropertyBinding(this, sPath, oContext, mParameters);
    return oBinding;
  };

  ServiceModel.prototype.bindList = function (sPath, oContext, aSorters, aFilters, mParameters) {
    var oBinding = new ServiceListBinding(this, sPath, oContext, aSorters, aFilters, mParameters);
    return oBinding;
  };

  ServiceModel.prototype.bindTree = function (sPath, oContext, aFilters, mParameters, aSorters) {
    var oBinding = new JSONTreeBinding(this, sPath, oContext, aFilters, mParameters, aSorters);
    return oBinding;
  };


  ServiceModel.prototype.setProperty = function (sPath, oValue, oContext, bAsyncUpdate) {
    var sResolvedPath = this.resolve(sPath, oContext),
      iArraySeparationSlash, sObjectPath, sProperty;

    // return if path / context is invalid
    if (!sResolvedPath) {
      return false;
    }

    // If data is set on root, throw error
    if (sResolvedPath == "/") {
      throw Error('Changes on the root node of this model are currently not supported');
      return false;
    }

    iArraySeparationSlash = sResolvedPath.indexOf('/', 1);

    sObjectPath = sResolvedPath.substring(0, iArraySeparationSlash);
    sProperty = sResolvedPath.substr(iArraySeparationSlash + 1);

    sProperty = sProperty.split('/').join('.');

    var oObject = this._getObject(sObjectPath);
    if (oObject && oObject._id) {
      var oParam = {};
      oParam[sProperty] = oValue;

      this.patch(oObject._id, oParam).then(
        function () {
          jQuery.sap.log.info('ServiceModel: Object with Id: ' + oObject._id + ' successfully patched');
        },
        function (oError) {
          jQuery.sap.log.warning('ServiceModel: Object with Id: ' + oObject._id + ' could not patched', oError);;
        }
      );

      return true;
    }
    return false;
  };

  ServiceModel.prototype.getProperty = function (sPath, oContext) {
    return this._getObject(sPath, oContext);
  };

  /**
   * @param {string} sPath
   * @param {object} [oContext]
   * @returns {any} the node of the specified path/context
   */
  ServiceModel.prototype._getObject = function (sPath, oContext) {
    var oNode = this.isLegacySyntax() ? this.oData : null;
    if (oContext instanceof Context) {
      oNode = this._getObject(oContext.getPath());
    } else if (oContext) {
      oNode = oContext;
    }
    if (!sPath) {
      return oNode;
    }
    var aParts = sPath.split("/"),
      iIndex = 0;
    if (!aParts[0]) {
      // absolute path starting with slash
      oNode = this.oData;
      iIndex++;
    }
    while (oNode && aParts[iIndex]) {
      oNode = oNode[aParts[iIndex]];
      iIndex++;
    }
    return oNode;
  };

  ServiceModel.prototype.isList = function (sPath, oContext) {
    var sAbsolutePath = this.resolve(sPath, oContext);
    return jQuery.isArray(this._getObject(sAbsolutePath));
  };


  return ServiceModel;

});
