sap.ui.define([
  'jquery.sap.global',
  'sap/ui/core/UIComponent',
  'sap/m/routing/Targets',
  'app/util/feathers/ServiceModel'
], function (jQuery, UIComponent, Targets, ServiceModel) {
  "use strict";

  var Component = UIComponent.extend("app.Component", {
    metadata: {
      includes: [
        "css/style.css"
      ],

      rootView: 'app.view.app',
    },

    app: undefined,

    models: {},

    init: function () {
      // 1. some very generic requires
      /*Nothing to require at the moment*/

      // 2. update global instance
      window['AppComponent'] = this;

      // 3. call overridden init (calls createContent)
      UIComponent.prototype.init.apply(this, arguments);
    },

    destroy: function () {
      // call overridden destroy
      UIComponent.prototype.destroy.apply(this, arguments);
    },

    createContent: function () {
      /* Connect to Feathers Backend */
      // Establish a Socket.io connection
      var socket = io();
      // Initialize our Feathers client application through Socket.io
      // with hooks and authentication.

      this.app = feathers()
        .configure(feathers.socketio(socket))
        .configure(feathers.hooks());

      // set address model
      this.models.address = new ServiceModel({
        app: this.app,
        servicename: 'addresses'
      });
      this.models.address.setDefaultBindingMode('TwoWay');
      this.setModel(this.models.address, 'address');

      return UIComponent.prototype.createContent.apply(this, arguments);
    }

  });

  return Component;

}, /* bExport= */ true);
